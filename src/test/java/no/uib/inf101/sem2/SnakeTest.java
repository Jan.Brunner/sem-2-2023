package no.uib.inf101.sem2;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SnakeTest {
@Test


/**
 * snakeSpawnTest() method
 * tests if the snake spawns in the correct location
 */

public void snakeSpawnTest(){
    Snake player = new Snake();
    assertEquals(player.getSnakeBody().get(0).getX(), GameModel.frameWidth / 2);
    assertEquals(player.getSnakeBody().get(0).getY(), GameModel.frameHight / 2);
    

}


/** snakeGrowTest() method
 * tests if the snake grows when it eats an apple
 * tests if the snake grows in the correct direction
 * tests if the snake grows in the correct location
 * 
 * 
 */
    public void snakeGrowTest() {
        Snake player = new Snake();
        Apple apple = new Apple(player);
        int snakeLength = player.getSnakeBody().size();
        player.grow();
        assertEquals(snakeLength + 1, player.getSnakeBody().size());
        assertEquals(player.getSnakeBody().get(0).getX(), player.getSnakeBody().get(1).getX() + GameModel.dimension);
        assertEquals(player.getSnakeBody().get(0).getY(), player.getSnakeBody().get(1).getY() );
    }
    
    
/**
 * snakeMoveTest() method
 * tests if the snake moves in the correct direction
 * tests if the snake moves in the correct location
 * 
 */
    public void snakeMoveTest() {
        Snake player = new Snake();
        player.move();
        assertEquals(player.getSnakeBody().get(0).getX(), player.getSnakeBody().get(1).getX() + GameModel.dimension);
        assertEquals(player.getSnakeBody().get(0).getY(), player.getSnakeBody().get(1).getY() );
    }
}




