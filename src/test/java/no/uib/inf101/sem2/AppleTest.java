
package no.uib.inf101.sem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class AppleTest {
    @Test
    /**
     * appleSpawnTest() method
     * tests if the apple spawns in a random location
     * tests if the apple spawns in a location that is not occupied by the snake
     * tests if the apple spawns in a location that is within the game board
     * for loop is used to run the test 100 times
     * 
     */
    public void appleSpawnTest() {
      
        for(int i = 0; i < 100; i++) {
            Snake player = new Snake();
            Apple apple = new Apple(player);
            assertEquals(true, apple.getAppleX() >= 0 && apple.getAppleX() < GameModel.width);
            assertEquals(true, apple.getAppleY() >= 0 && apple.getAppleY() < GameModel.height);
            assertEquals(true, !player.getSnakeBody().contains(apple));
        }
      
    }
    
}
