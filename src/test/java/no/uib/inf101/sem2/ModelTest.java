package no.uib.inf101.sem2;
import no.uib.inf101.sem2.GameModel;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Timer;

import org.junit.jupiter.api.Test;

public class ModelTest {

    
   /**
    * checkCollisionTest()
    * Checks if the game ends when the snake collides with the wall
    * Steers the snake into the wall and checks if the game ends
    * assert that the game state is END
    */
    @Test
    public void checkCollisionTest() {
      GameModel model = new GameModel();
    
      model.getPlayer().right();
      model.start();
    
      for (int i = 0; i < 100; i++) {
        model.update();
      }
    
      assertEquals(GameState.END, model.getGameState());
    }
        }
    
            
    
      
    
     

