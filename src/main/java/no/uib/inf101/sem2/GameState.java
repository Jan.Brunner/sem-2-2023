package no.uib.inf101.sem2;

public enum GameState {
     START, RUNNING, END
}
