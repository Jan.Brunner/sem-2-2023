package no.uib.inf101.sem2;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class GameView extends JPanel implements ActionListener{
	
	private Timer timer;
	
	
	private Snake snake;
	private Apple apple;
	private GameModel gameModel;
	private GameController gameController;
	
	public GameView(GameModel gameModel) {
		this.gameModel = gameModel;
		this.timer = new Timer(100, this);
		this.snake = gameModel.getPlayer();

	}
	
	public void paintComponent(java.awt.Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;
		
		

		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, GameModel.frameWidth  , GameModel.frameHight );
		
		if(gameModel.gameState == GameState.START) {
			g2d.setColor(Color.white);
			g2d.drawString("Press ANY for EASY, 1 for NORMAL or 2 for HARD", GameModel.width/2 * GameModel.dimension - 150, GameModel.height / 2 * GameModel.dimension - 20);
		}
		else if(gameModel.gameState == GameState.RUNNING) {
			g2d.setColor(Color.red);
			g2d.fillRect(gameModel.getApple().getAppleX() * GameModel.dimension, gameModel.getApple().getAppleY() * GameModel.dimension, GameModel.dimension, GameModel.dimension);
		
			g2d.setColor(Color.green);
			for(Rectangle r : snake.getSnakeBody()) {
				g2d.fill(r);
			}
		}
		else {
			g2d.setColor(Color.white);
			g2d.drawString("Your Score: " + (snake.getSnakeBody().size() - 3), GameModel.width/2 * GameModel.dimension - 40, GameModel.height / 2 * GameModel.dimension - 20);
		}
	}

	/**
	 * actionPerformed method
	 * is called every time the timer updates
	 * calls the repaint method and the update method in the game class
	 * @param e ActionEvent
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
		gameModel.update();
	}
	/**
	 * start method
	 * sets the delay of the timer depending on the difficulty
	 * starts the timer
	 */
	public void start() {
		int difficulty = gameModel.getDifficulty();
		if(difficulty == 1) {
		  timer.setDelay(100);
		} else if(difficulty == 2) {
		  timer.setDelay(50);
		} else {
		  timer.setDelay(25);
		}
		timer.start();
	  }
	
}
