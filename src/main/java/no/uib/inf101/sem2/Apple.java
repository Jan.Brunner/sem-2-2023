package no.uib.inf101.sem2;

import java.awt.Rectangle;

public class Apple {
	private int appleX;
	private int AppleY;
	
	public Apple(Snake player) {
		this.randomSpawn(player);
	}
	
	/**
	 * randomSpawn() method
	 * spawns the apple in a random location
	 * the apple can not spawn in the same location as the snake
	 * @param player
	 */
	public void randomSpawn(Snake player) {
		boolean spawn = false;
		while(!spawn) {
			appleX = (int) (Math.random() * GameModel.width);
			AppleY = (int) (Math.random() * GameModel.height);
			
			Rectangle apple = new Rectangle(GameModel.dimension, GameModel.dimension);
			apple.setLocation(appleX * GameModel.dimension, AppleY * GameModel.dimension);
			
			if(!player.getSnakeBody().contains(apple)) {
				spawn = true;
			}
		}
	}

	public int getAppleX() {
		return appleX;
	}

	public void setAppleX(int x) {
		this.appleX = x;
	}

	public int getAppleY() {
		return AppleY;
	}

	public void setAppleY(int y) {
		this.AppleY = y;
	}
}
