package no.uib.inf101.sem2;
import javax.swing.JFrame;
import no.uib.inf101.sem2.GameModel;



public class Main {
  private GameModel model;
  private GameView view;
  public static void main(String[] args) {
    GameModel model = new GameModel();
    GameView view = new GameView(model);

    JFrame frame = new JFrame();

    frame.add(view);
    frame.addKeyListener(new GameController(model, view));
    frame.setTitle("Snake Game");
    frame.setSize(model.frameWidth + model.dimension , model.frameHight + 45);
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setFocusable(true);
    frame.requestFocus();
  }

    
  }

