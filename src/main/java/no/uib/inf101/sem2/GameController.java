package no.uib.inf101.sem2;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameController implements KeyListener {

    private GameView gameView;
    private GameModel gameModel;
   

    public GameController(GameModel gameModel, GameView gameView) {
        this.gameModel = gameModel;
        this.gameView = gameView;
        
    }



    /**
	 * Checks if the player has pressed a key
	 * Choose difficulty if the game is in start state
	 * If the player has pressed a key the player will move in the direction of the key
	 * The snake into itself (the player can't move in the opposite direction of the current move)
	 */
    public void keyPressed(KeyEvent e) {
        
		int keyCode = e.getKeyCode();
	  
		if(keyCode == KeyEvent.VK_1 && gameModel.gameState == GameState.START) {
		  gameModel.setDifficulty(2);
		}
	  
		if(keyCode == KeyEvent.VK_2 && gameModel.gameState == GameState.START) {
		  gameModel.setDifficulty(3);
		}
			  
		if(gameModel.gameState == GameState.RUNNING) {
		  if(keyCode == KeyEvent.VK_W && gameModel.getPlayer().getMove() != Direction.DOWN) {
			gameModel.getPlayer().up();
		  }
			  
		  if(keyCode == KeyEvent.VK_S && gameModel.getPlayer().getMove() != Direction.UP) {
			gameModel.getPlayer().down();
		  }
			  
		  if(keyCode == KeyEvent.VK_A && gameModel.getPlayer().getMove() != Direction.RIGHT) {
			gameModel.getPlayer().left();
		  }
			  
		  if(keyCode == KeyEvent.VK_D && gameModel.getPlayer().getMove() != Direction.LEFT) {
			gameModel.getPlayer().right();
		  }
		} else {
		  gameModel.getPlayer().right();
		  gameModel.start();
		  gameView.start();
		}
	  }
    

    @Override
    public void keyReleased(KeyEvent arg0) {
   
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
        
    }

    

}