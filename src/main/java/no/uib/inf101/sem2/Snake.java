package no.uib.inf101.sem2;

import java.awt.Rectangle;
import java.util.ArrayList;

public class Snake {
	private  ArrayList<Rectangle> snakeBody;
	private int width = GameModel.width;
	private int height = GameModel.height;
	private int dimension = GameModel.dimension;
	private Direction direction; //UP, DOWN, LEFT, RIGHT, NONE
	
	
	/** 
	 * Snake constructor
	 * initializes the snakeBody arraylist
	 * creates 3 rectangles and adds them to the snakeBody arraylist
	 * sets the location of the rectangles
	 * sets the move to "NONE"
	 */
	public Snake() {
		snakeBody = new ArrayList<>();
        Rectangle Body = new Rectangle();


		
		for (int i = 0; i < 3; i++) {
            Body = new Rectangle(dimension, dimension);
            Body.setLocation((width / 2 - 1 * i) * dimension, (height / 2) * dimension);
            snakeBody.add(Body);
        }
		
		
		
		direction = Direction.NONE;
	}
	
	/**
	 * move() method
	 * moves the snake in the direction of the move variable
	 * if the move variable is "NONE" the snake does not move
	 * the snake moves in the direction of the move variable by adding a new rectangle to the snakeBody arraylist 
	 * in the direction of the move variable and removing the last rectangle in the snakeBody arraylist
	 */
	public void move() {
		if(direction != Direction.NONE) {
			Rectangle snakeHead = snakeBody.get(0);
			
			Rectangle tempBody = new Rectangle(dimension, dimension);
			
			if(direction == Direction.UP) {
				tempBody.setLocation(snakeHead.x, snakeHead.y - dimension);
			}
			else if(direction == Direction.DOWN) {
				tempBody.setLocation(snakeHead.x, snakeHead.y + dimension);
			}
			else if(direction == Direction.LEFT) {
				tempBody.setLocation(snakeHead.x - dimension, snakeHead.y);
			}
			else{
				tempBody.setLocation(snakeHead.x + dimension, snakeHead.y);
			}
			
			snakeBody.add(0, tempBody);
			snakeBody.remove(snakeBody.size()-1);
		}
	}
	
	/**
	 * grow() method
	 * adds a new rectangle to the snakeBody arraylist in the direction of the move variable
	 */
	public void grow() {
		Rectangle snakeHead = snakeBody.get(0);
		
		Rectangle Body = new Rectangle(dimension, dimension);
		
		if(direction == Direction.UP) {
			Body.setLocation(snakeHead.x, snakeHead.y - dimension);
		}
		else if(direction == Direction.DOWN) {
			Body.setLocation(snakeHead.x, snakeHead.y + dimension);
		}
		else if(direction == Direction.LEFT) {
			Body.setLocation(snakeHead.x - dimension, snakeHead.y);
		}
		else{
			Body.setLocation(snakeHead.x + dimension, snakeHead.y);
		}
		
		snakeBody.add(0, Body);
	}

	public ArrayList<Rectangle> getSnakeBody() {
		return snakeBody;
	}
	

	public void setsnakeBody(ArrayList<Rectangle> snakeBody) {
		this.snakeBody = snakeBody;
	}
	
	public int getX() {
		return snakeBody.get(0).x;
	}
	
	public int getY() {
		return snakeBody.get(0).y ;
	}
	
	public Direction getMove() {
		return direction;
	}
	
	/**
	 * up(), down(), left(), right() methods
	 * sets the move variable to the direction of the method
	 */
	public void up() {
		direction = Direction.UP;
	}
	
	public void down() {
		direction = Direction.DOWN;
	}
	public void left() {
		direction = Direction.LEFT;
	}
	public void right() {
		direction = Direction.RIGHT;
	}


	
		

}
