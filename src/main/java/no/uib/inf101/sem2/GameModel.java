package no.uib.inf101.sem2;



import javax.swing.JFrame;

public class GameModel {

	private Snake player;
	private Apple apple;
	
	public GameState gameState = GameState.START;
	
	private JFrame frame;

	public int difficulty;

	// 30 x 30 grid each tile 20 x 20 pixels 
	public static final int width = 30; 
	public static final int height = 30;
	public static final int dimension = 20;

	// Frame high and width are the width and height of the frame in pixels times the dimension of each tile 600 x 600 pixels

    public static final int frameHight = height * dimension;
    public static final int frameWidth = width * dimension;
	/**
	 * Game constructor 
	 * initializes the frame, player, apple and graphics
	 * adds the graphics to the frame
	 * sets the title, size, visibility and close operation of the frame
	 */
	public GameModel() {
		this.player = new Snake();
		this.apple = new Apple(player);	
		difficulty = 1;
	}
	
	public void start() {
		this.gameState = GameState.RUNNING;
	}
	
	/**
	 * Updates the game
	 * If the game is running the player is moved and if the player has collided with the apple 
	 * the player grows and a new apple is spawned
	 * If the player has collided with the wall or itself the game ends		
	 * other wise the player keeps moveding
	 */
	
	public void update() {
		if(this.gameState == GameState.RUNNING) {
			if(checkAppleCollision()) {
				player.grow();
				apple.randomSpawn(player);
			}
			else if(checkWallCollision() || checkSnakeCollision()) {
				this.gameState = GameState.END;
			}
			else {
				player.move();
			}
		}
	}
	/**
	 * Checks if the player has collided with the wall
	 * If the snakes head is outside the frame the player has collided with the wall
	 * @return true if the player has collided with the wall
	 */
	public boolean checkWallCollision() {
		if(player.getX() < 0 || player.getX() >= width * dimension 
				|| player.getY() < 0|| player.getY() >= height * dimension ) {
			return true;
		}
		return false;
	}
	/**
	 * Checks if the player has collided with the apple
	 * If the snakes head is on the same tile as the apple the player has collided with the apple
	 * @return true if the player has collided with the apple
	 
	 */
	private boolean checkAppleCollision() {
		if(player.getX() == apple.getAppleX() * dimension && player.getY() == apple.getAppleY() * dimension) {
			return true;
		}
		return false;
	}
	/**
	 * Checks if the player has collided with itself
	 * If the snakes head is on the same tile as any other part of the snake the player has collided with itself
	 * @return true if the player has collided with itself
	 */
	private boolean checkSnakeCollision() {
		for(int i = 1; i < player.getSnakeBody().size(); i++) {
			if(player.getX() == player.getSnakeBody().get(i).x &&
					player.getY() == player.getSnakeBody().get(i).y) {
				return true;
			}
		}
		return false;
	}

	
	

	
	
	

	public Snake getPlayer() {
		return player;
	}

	public void setPlayer(Snake player) {
		this.player = player;
	}

	public Apple getApple() {
		return apple;
	}

	public void setApple(Apple apple) {
		this.apple = apple;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

    public Object getGameState() {
        return this.gameState;
    }

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public void setDifficulty(int difficulty){
		this.difficulty = difficulty;
	  }

	public int getDifficulty(){
		return difficulty;
	  }
}
