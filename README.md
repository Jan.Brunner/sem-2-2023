# Mitt program

Se [oppgaveteksten](./OPPGAVETEKST.md) til semesteroppgave 2. Denne README -filen kan du endre som en del av dokumentasjonen til programmet, hvor du beskriver for en bruker hvordan programmet skal benyttes.

Yt link https://www.youtube.com/shorts/kcXqCdxAqG4 

Short description of the project
Simple snake game 
Click run on the main class to start the game.
You will be faced with a small frame with a text popping up asking you to choose a difficulty.
Press any button for easy, 1 for normal and 2 for hard.
After choosing a difficulty the game will start
You are controlling a green snake with the WASD keys
The goal is to eat as many apples as possible without hitting the walls or yourself.
The game will end when you hit the wall or yourself.
If that happens you will be given a score based on how many apples you ate.
